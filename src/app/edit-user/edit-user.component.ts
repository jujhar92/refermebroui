import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { StorageService } from '../storage.service';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  @ViewChild(ModalWindowComponent) modalWindowComponent;
  userName: string;
  userEmail: string;
  userCompany: string;
  userDesignation: string;
  resume: File;

  constructor(private storageService: StorageService, private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  fileEvent($event) {
    const fileSelected: File = $event.target.files[0];
    this.resume = fileSelected;
  }

  updateDetails() {
    if(this.userCompany == undefined || this.userCompany == null){
      this.toastService.warning("Please enter your company name");
      return;
    }
    else if(this.userDesignation == undefined || this.userDesignation == null){
      this.toastService.warning("Please enter your designation");
      return;
    }
    this.spinner.show();
    var data = {
      "emailAddress": this.userEmail,
      "company": this.userCompany,
      "designation": this.userDesignation
    };
    const _formData = new FormData();
    _formData.append('resume', this.resume, this.resume.name);
    _formData.append('company', this.userCompany);
    _formData.append('emailAddress', this.userEmail);
    _formData.append('designation', this.userDesignation);


    this.userService.updateUser(_formData).subscribe(
      res => {
        if (res && res["status"]) {
          this.spinner.hide();
          this.toastService.success("Details successfully updated");
          this.hide();
        }
        else if (res && res["status"] == "403") {
          this.spinner.hide();
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
        else{
          this.spinner.hide();
        }
      },
      err => {
        this.spinner.hide();
        this.toastService.error("error occured while updating your details, Please try after some time");
      }
    );

  }
  setValues() {
    this.userName = this.storageService.getItem("maneetu");
    this.userEmail = this.storageService.getItem("maneete");
    this.userService.getEditDetails(this.userEmail).subscribe(
      res => {
        if (res && res["status"]) {
          this.userCompany = res["company"];
          this.userDesignation = res["designation"];
        }
        else if (res && res["status"] == "403") {
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
      },
      err => {
        this.toastService.error("error occured while fetching your details, Please try after some time");
      }
    );
  }
  show() {
    this.setValues();
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }

}
