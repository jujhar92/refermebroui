import { Component, OnInit, ViewChild } from '@angular/core';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { JobComponent } from '../job/job.component';
import { ResetpasswordComponent } from '../resetpassword/resetpassword.component';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {



  @ViewChild(LoginComponent) loginModalWindow;
  @ViewChild(SignupComponent) signUpModalWindow;
  @ViewChild(EditUserComponent) editUserComponent;
  @ViewChild(ChangePasswordComponent) changePasswordComponent;
  @ViewChild(JobComponent) jobComponent;
  @ViewChild(ResetpasswordComponent) resetpasswordComponent;

  constructor() { }

  ngOnInit() {
    //this.showEditModalWindow();
  }
  showChangePasswordModalWindow() {
    this.changePasswordComponent.show();

  }
  showRestPasswordModalWindow(){
    this.resetpasswordComponent.show();
  }
  showLoginModalWindow() {
    this.loginModalWindow.show();

  }
  hideLoginModalWindow() {
    this.loginModalWindow.hide();
  }
  showEditModalWindow() {
    this.editUserComponent.show();

  }
  hideEditModalWindow() {
    this.editUserComponent.hide();
  }
  showSignUpModalWindow() {
    this.signUpModalWindow.show();
  }
  showJobPostModalWindow(){
    this.jobComponent.show();
  }

}
