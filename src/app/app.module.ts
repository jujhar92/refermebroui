import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { TopBannerComponent } from './top-banner/top-banner.component';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { LoginComponent } from './login/login.component';
import { ModalWindowComponent } from './modal-window/modal-window.component';
import { SignupComponent } from './signup/signup.component';
import { ConfigService } from './config.service';
import { UserService } from './user.service';
import { EditUserComponent } from './edit-user/edit-user.component';
import { StorageService } from './storage.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UserListComponent } from './user-list/user-list.component';
import { JobComponent } from './job/job.component';
import { FilterjobsComponent } from './filterjobs/filterjobs.component';
import { PostedjobComponent } from './postedjob/postedjob.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LandingPageComponent,
    TopBannerComponent,
    LoginComponent,
    ModalWindowComponent,
    SignupComponent,
    EditUserComponent,
    ChangePasswordComponent,
    UserListComponent,
    JobComponent,
    FilterjobsComponent,
    PostedjobComponent,
    ResetpasswordComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot({
      timeOut: 3000
    }),
    NgxSpinnerModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [ConfigService, UserService, StorageService, NgxSpinnerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
