import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { UserService } from '../user.service';
import { unescapeIdentifier } from '@angular/compiler';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userName: string;
  userEmail: string;
  userPassword: string;
  userConfirmPassword: string;
  userCompany: string;
  userDesignation: string;
  resume: File;

  constructor(private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  @ViewChild(ModalWindowComponent) modalWindowComponent;

  ngOnInit() {
  }

  show() {
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }

  validateInputs() {
    var message;
    if (this.userName == undefined) {
      message = "Please provide a user name";
    }
    else if (this.userEmail == undefined) {
      message = "Please provide your email";
    }
    else if (this.userPassword == undefined) {
      message = "Please provide a password";
    }
    else if (this.userCompany == undefined) {
      message = "Please provide your company";
    }
    else if (this.userDesignation == undefined) {
      message = "Please provide your designation";
    }
    return message;
  }
  fileEvent($event) {
    const fileSelected: File = $event.target.files[0];
    this.resume = fileSelected;
  }

  registerUser() {
    var message = this.validateInputs();
    if (message != undefined) {
      this.toastService.error(message);
      return;
    }
    this.spinner.show();
    var data = {
      "userName": this.userName,
      "emailAddress": this.userEmail,
      "password": this.userPassword,
      "company": this.userCompany,
      "designation": this.userDesignation
    };
    const _formData = new FormData();
    _formData.append('resume', this.resume, this.resume.name);
    _formData.append('userName', this.userName);
    _formData.append('password', this.userPassword);
    _formData.append('company', this.userCompany);
    _formData.append('emailAddress', this.userEmail);
    _formData.append('designation', this.userDesignation);

    this.userService.registerUser(_formData).subscribe(
      res => {
        this.spinner.hide();
        if (res["status"]) {
          this.toastService.success("Successfully registered, We have sent you an confirmation email on your registered email please verify for accound activation");
          this.hide();
        }
        else {
          this.toastService.error(res["message"]);
        }
      },
      err => {
        this.spinner.hide();
        this.toastService.error(err.message);
      });
  }

}
