import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { StorageService } from '../storage.service';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent implements OnInit {

  @ViewChild(ModalWindowComponent) modalWindowComponent;
  jobTitle: string;
  jobLocation: string = "Gurgaon";
  requiredSkills: string;
  requiredMinExperience: Number;
  //requiredMaxExperience: string;
  isJobAdStillValid:boolean;
  jobID:Number;

  constructor(private storageService: StorageService, private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }
  selecJobLocation($event){
    this.jobLocation = $event.currentTarget.value;
  }
  validate(){
    if(this.jobTitle == null || this.jobTitle == undefined){
      this.toastService.warning("Please enter job title");
      return false;
    }
    else if(this.jobLocation == null || this.jobLocation == undefined){
      this.toastService.warning("Please enter job location");
      return false;
    }
    else if(this.requiredMinExperience == null || this.requiredMinExperience == undefined){
      this.toastService.warning("Please enter required min experience for this job");
      return false;
    }
    else if(this.requiredSkills == null || this.requiredSkills == undefined){
      this.toastService.warning("Please enter required key skills for this job");
      return false;
    }
    return true;
  }
  postJob(){
    if(!this.validate()){
      return;
    }
    this.spinner.show();
    var jobValid = "Y";
    if(this.isJobAdStillValid != undefined && this.isJobAdStillValid){
      jobValid = "N";
    }
    var data = {
      "emailAddress":this.storageService.getItem("maneete"),
      "postedJobs":[{
        "id":this.jobID,
        "jobTitle":this.jobTitle,
        "requiredKeySkills":this.requiredSkills,
        "jobLocation":this.jobLocation,
        "minExp":this.requiredMinExperience,
        "isPositionStillOpen":jobValid
      }]
    };
    this.userService.postJob(data).subscribe(
      res => {
        this.spinner.hide();
        if (res && res["status"]) {
          this.toastService.success(res["message"]);
          this.hide();
        }
        else if (res && res["status"] == false) {
          this.toastService.warning(res["message"]);         
        }
        else if (res && res["status"] == "403") {
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
      },
      err => {
        this.spinner.hide();
        this.toastService.error("error occured while fetching your details, Please try after some time");
      }
    );
  }
  setValues(){
    this.userService.getJobDetails(this.storageService.getItem("maneete")).subscribe(
      res => {
        if (res && res["status"] && res["foundJob"]) {
          this.jobID = res["id"];
          this.jobTitle = res["jobTitle"];
          this.jobLocation = res["jobLocation"];
          if(res["isJobAdStillValid"] == "Y")
          {
            this.isJobAdStillValid = true;
          }
          else
          {
            this.isJobAdStillValid = false;
          }          
          this.requiredSkills = res["requiredKeySkills"];
          //var experience = res["requiredExperience"].split("-");
          this.requiredMinExperience = res["requiredExperience"];
          //this.requiredMaxExperience = experience[1].replace("years","").trim();
        }
        else if (res && res["status"] == false) {
          this.toastService.warning(res["message"]);
          this.userService.logout();
        }
        else if (res && res["status"] == "403") {
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
      },
      err => {
        this.toastService.error("error occured while fetching your details, Please try after some time");
      }
    );
  }

  show() {
    this.setValues();
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }

}
