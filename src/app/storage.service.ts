import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  setItem(key, value) {
    if (window.sessionStorage) {
      window.sessionStorage.setItem(key, value);
    }
  }
  getItem(key) {
    if (window.sessionStorage) {
      return window.sessionStorage.getItem(key);
    }
  }
  clear(){
    if(window.sessionStorage){
      window.sessionStorage.clear();
    }
  }
}
