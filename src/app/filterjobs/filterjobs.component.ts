import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from '../storage.service';
import { UserService } from '../user.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-filterjobs',
  templateUrl: './filterjobs.component.html',
  styleUrls: ['./filterjobs.component.css']
})
export class FilterjobsComponent implements OnInit {

  isDelhi:boolean;
  isGurgaon:boolean;
  isNoida:boolean;
  isBanglore:boolean;
  isHyderbad:boolean;
  isPune:boolean;
  isChennai:boolean;
  isAll:boolean;
  minExp:Number = 0;
  postedJobs = [];
  @Output() filteredJobFetched = new EventEmitter();

  constructor(private storageService: StorageService, private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  createWhereClause(){
    
  }

  filterJobs($event){
    this.spinner.show();
    var data = {
      "jobLocations":[],
      "minExp":this.minExp
    };
    if(this.isAll){
      data.jobLocations.push("All");
    }
    else{
      if(this.isDelhi){
        data.jobLocations.push("Delhi");
      }
      if(this.isGurgaon){
        data.jobLocations.push("Gurgaon");
      }
      if(this.isNoida){
        data.jobLocations.push("Noida");
      }
      if(this.isBanglore){
        data.jobLocations.push("Banglore");
      }
      if(this.isHyderbad){
        data.jobLocations.push("Hyderabad");
      }
      if(this.isChennai){
        data.jobLocations.push("Chennai");
      }
      if(this.isPune){
        data.jobLocations.push("Pune");
      }
    }

    this.userService.filterJobs(data).subscribe(
      res=>{
        if (res) {
          this.spinner.hide();
          var len = res["length"];          
          this.postedJobs = [];
          for(let i=0;i<len;i++){
            var postedJob = res[i];
            this.postedJobs.push(postedJob);
          }
          this.filteredJobFetched.emit();
        }
        else if (res && res["status"] == "403") {
          this.spinner.hide();
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
        else{
          this.spinner.hide();
        }
      },
      err=>{
        this.spinner.hide();
        this.toastService.error("error occured while fetching posted jobs, Please try after some time");
      }
    );
    
  }

}
