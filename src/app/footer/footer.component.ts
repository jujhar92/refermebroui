import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  appName: string = "Refer UI"

  constructor(private configService: ConfigService) {
    this.configService.getConfig().subscribe(
      data => { this.appName = data["project"]["appName"]; }

    );
  }

  ngOnInit() {
  }

}
