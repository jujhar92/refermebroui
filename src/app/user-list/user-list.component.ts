import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService } from '../storage.service';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { FilterjobsComponent } from '../filterjobs/filterjobs.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  postedJobs = [];
  @ViewChild(FilterjobsComponent) filterjobsComponent;
  

  constructor(private storageService: StorageService, private userService: UserService, private toastService: ToastrService) { }

  ngOnInit() {
    this.loadJobs();
  }
  updatePostedJobs(){
    this.postedJobs = this.filterjobsComponent.postedJobs;
  }

  loadJobs(){
    this.userService.listJobs(this.storageService.getItem("maneete")).subscribe(
      res => {
        if (res) {
          var len = res["length"];          
          for(let i=0;i<len;i++){
            var postedJob = res[i];
            this.postedJobs.push(postedJob);
          }
        }
        else if (res && res["status"] == "403") {
          this.toastService.warning("Session timeout");
          this.userService.logout();
        }
      },
      err => {
        this.toastService.error("error occured while fetching posted jobs, Please try after some time");
      }
    );
  }

}
