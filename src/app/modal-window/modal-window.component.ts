import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.css']
})
export class ModalWindowComponent implements OnInit {

  @Input() header: string;
  @Input() id: string;



  constructor() { }

  hideModal() {
    window['$']("#" + this.id).modal('hide');
  }
  showModal() {
    window['$']("#" + this.id).modal('show');

  }

  ngOnInit() {
  }

}
