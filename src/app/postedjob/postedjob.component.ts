import { Component, OnInit, Input } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from '../storage.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-postedjob',
  templateUrl: './postedjob.component.html',
  styleUrls: ['./postedjob.component.css']
})
export class PostedjobComponent implements OnInit {

  @Input() companyName:string;
  @Input() jobTitle:string;
  @Input() requiredExperience:string;
  @Input() jobLocation:string;
  @Input() requiredSkills:string;
  @Input() jobExpAndLoc:string;
  @Input() userEmailAddress:string;
  isUserLoggedIn = false;

  constructor(private userService: UserService, private toastService: ToastrService,private storageService: StorageService,private spinner: NgxSpinnerService) {
    this.userService.isUserLoggedIn().subscribe(d=>{
      this.isUserLoggedIn = d;
    });
   }

  ngOnInit() {
  }
  applyForJob(){
    if(this.isUserLoggedIn){
      this.spinner.show();
      var data = {
        "toEmailAddress":this.userEmailAddress,
        "fromEmailAddress":this.storageService.getItem("maneete"),
        "jobPosition":this.jobTitle
      };
      this.userService.referMe(data).subscribe(
        res=>{
          this.spinner.hide();
          this.toastService.success("Sent referral");
        },
        err=>{
          this.spinner.hide();
          this.toastService.error("Not able to refer you, please try after some time");
        }
      );
    }
    else{
      this.toastService.warning("Please log in for applying a job");
    }
  }

}
