import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { UserService } from '../user.service';
import { StorageService } from '../storage.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  @ViewChild(ModalWindowComponent) modalWindowComponent;
  userName: string;
  userEmail: string;
  currentPassword: string;
  newPassword: string;
  confirmNewPassword: string;
  updatePasswordErrorMessage: string;
  updatePasswordError: boolean = false;

  constructor(private storageService: StorageService, private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  setValues() {
    this.userName = this.storageService.getItem("maneetu");
    this.userEmail = this.storageService.getItem("maneete");
  }
  validateInputs() {
    var message;
    if (this.currentPassword == undefined) {
      message = "Please enter current password";
    }
    else if (this.newPassword == undefined) {
      message = "Please enter new password";
    }
    else if (this.confirmNewPassword == undefined) {
      message = "Please enter confirmation password";
    }
    else if (this.newPassword != this.confirmNewPassword) {
      message = "New password dosen't match with confirmation password";
    }
    return message;
  }
  updatePassword() {
    var message = this.validateInputs();
    if (message != undefined) {
      this.toastService.error(message, "Change Password");
      return;
    }
    this.spinner.show();
    var data = {
      "emailAddress": this.userEmail,
      "currentPassword": this.currentPassword,
      "newPassword": this.newPassword,
      "confirmNewPassword": this.confirmNewPassword
    };
    this.userService.updatePassword(data).subscribe(
      res => {
        if (res && res["status"] == true) {
          this.spinner.hide();
          this.toastService.success(res["message"], "Change Password");
          this.hide();
        }
        else if (res && res["status"] == false) {
          this.spinner.hide();
          this.toastService.error(res["message"], "Change Password");
        }
        else if (res && res["status"] == "403") {
          this.spinner.hide();
          this.hide();
          this.toastService.warning("session expired!", "Login");
          this.userService.logout();
        }
        else{
          this.spinner.hide();
        }
      },
      err => {
        this.spinner.hide();
        this.toastService.error(err.message, "Change Password");
      }
    );


  }
  show() {
    this.setValues();
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }

}
