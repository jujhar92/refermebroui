import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ModalWindowComponent } from '../modal-window/modal-window.component';
import { UserService } from '../user.service';
import { StorageService } from '../storage.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-login-component',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild(ModalWindowComponent) modalWindowComponent;
  @Output() showSignUpWindow = new EventEmitter();
  @Output() showRestPasswordWindow = new EventEmitter();
  userEmail: string;
  userPassword: string;
  rememberMe: boolean;
  loginErrorMessage: string;
  loginError: boolean = false;

  constructor(private userService: UserService, private storageService: StorageService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {

  }
  validateInputs() {
    var message;
    if (this.userEmail == undefined) {
      message = "Please provide your email to login";
    }
    else if (this.userPassword == undefined) {
      message = "Please provide your password to login";
    }
    return message;
  }

  login() {
    var message = this.validateInputs();
    if (message != undefined) {
      this.toastService.error(message);
      return;
    }
    this.spinner.show();
    var data = {
      "emailAddress": this.userEmail,
      "password": this.userPassword
    };
    this.userService.login(data).subscribe(
      res => {
        this.spinner.hide();
        if (res["isAuthenticated"] && res["status"]) {
          this.loginError = false;
          this.userService.loggedIn.next(true);
          this.storageService.setItem("maneett", res["jwtToken"]);
          this.storageService.setItem("maneete", data.emailAddress);
          this.storageService.setItem("maneetu", res["userName"]);
          this.hide();
          this.toastService.success("Succussfully loggedIn");
        }
        else if(!res["isAuthenticated"] && res["status"]!="403"){
          this.toastService.error(res["message"]);
        }
        else {
          this.userService.loggedIn.next(false);
          this.loginError = true;
          this.loginErrorMessage = res["message"];
        }
      },
      err => {
        this.spinner.hide();
        this.userService.loggedIn.next(false);
        this.loginError = true;
        this.loginErrorMessage = err.message;
      });
  }

  show() {
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }

  emitSignUpWindowEvent() {
    this.showSignUpWindow.emit();
  }
  emitRestPasswordEvent(){
    this.showRestPasswordWindow.emit();
  }

}
