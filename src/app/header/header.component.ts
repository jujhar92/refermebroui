import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConfigService } from '../config.service';
import { UserService } from '../user.service';
import { StorageService } from '../storage.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() openLoginPage = new EventEmitter();
  @Output() openEditPage = new EventEmitter();
  @Output() openChangePasswordPage = new EventEmitter();
  @Output() openJobPostPage = new EventEmitter();

  appName = 'Refer UI';
  isUserLoggedIn = true;


  constructor(private configService: ConfigService, private userService: UserService, private storageService: StorageService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loadConfig();
    this.userService.isUserLoggedIn().subscribe(d => {
      this.isUserLoggedIn = d;
    });

    //check for navigation time API support
    if (window.performance) {
      console.info("window.performance work's fine on this browser");
      if (window.performance.navigation.type == window.performance.navigation.TYPE_RELOAD) {
        if (this.storageService.getItem("maneett") != undefined && this.storageService.getItem("maneete") != undefined) {
          this.userService.loggedIn.next(true);
        }

        console.info("This page is reloaded" + window.performance.navigation.TYPE_RELOAD);
      } else {
        console.info("This page is not reloaded" + window.performance.navigation.TYPE_RELOAD);
      }
    }




  }
  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }
  emitLoginPageEvent() {
    this.openLoginPage.emit();
  }

  emitEditPageEvent() {
    this.openEditPage.emit();
  }
  emitChangePasswordEvent() {
    this.openChangePasswordPage.emit();
  }
  emitJobPostPageEvent(){
    this.openJobPostPage.emit();
  }
  logout() {
    this.userService.logout();
    this.toastr.success("Successfully logout");
  }

  loadConfig() {
    this.configService.getConfig()
      .subscribe(
        data => {
          this.appName = data["project"]['appName'];
        }
      );
  }

}
