import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from '../user.service';
import { ModalWindowComponent } from '../modal-window/modal-window.component';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  emailAddress:string;
  newPassword:string;
  OTP:string;
  isOTPGenerated:boolean = false;
  @ViewChild(ModalWindowComponent) modalWindowComponent;


  constructor(private userService: UserService, private toastService: ToastrService,private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }
  
  validateInputs(){
    if(this.emailAddress == null || this.emailAddress == undefined){
      this.toastService.warning("Please provided registered Email ID");
      return false;
    }
    else if(this.OTP == null || this.OTP == undefined){
      this.toastService.warning("Please provide OTP sent to your registered Email ID");
      return false;
    }
    else if(this.newPassword == null || this.newPassword == undefined){
      this.toastService.warning("Please provide new password");
      return false;
    }
    return true;

  }

  show() {
    this.modalWindowComponent.showModal();

  }
  hide() {
    this.modalWindowComponent.hideModal();
  }
  
  resetPassword(){
    if(!this.validateInputs()){
      return;
    }
    this.spinner.show();
    var data = {
      "emailAddress":this.emailAddress,
      "otp":this.OTP,
      "newPassword":this.newPassword
    };
    this.userService.resetPassword(data).subscribe(
      res =>{
        this.spinner.hide();
        this.toastService.success("Password reset");
      },
      err => {
        this.spinner.hide();
        this.toastService.error(err);
      }
    );
  }
  generateOTP(){
    if(this.emailAddress == null || this.emailAddress == undefined){
      this.toastService.warning("Please provided registered Email ID");
      return;
    }
    this.spinner.show();
    this.userService.sendOTP(this.emailAddress).subscribe(
      res =>{
        this.spinner.hide();
        this.toastService.success("Sent OTP");
        this.isOTPGenerated = true;
      },
      err => {
        this.spinner.hide();
        this.toastService.error(err);
      }
    );

  }

}
