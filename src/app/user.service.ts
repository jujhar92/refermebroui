import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { StorageService } from './storage.service';

@Injectable()
export class UserService {

  loggedIn = new BehaviorSubject(false);
  baseUrl: string = "http://localhost:8080/";

  constructor(private configService: ConfigService, private http: HttpClient, private storageService: StorageService) {

    this.configService.getConfig().subscribe(
      data => { this.baseUrl = data["project"]["baseUrl"]; }

    );
  }

  getHeader() {
    var token = this.storageService.getItem("maneett");
    const headers = new HttpHeaders().set("Authorization", token);
    
    
    //headers.append("Authorization", token);
    //headers.set("Authorization", token);
    return headers;
  }

  isUserLoggedIn() {
    return this.loggedIn.asObservable();
  }

  login(data) {
    return this.http.post(this.baseUrl + "users/login", data);

  }
  logout() {
    this.storageService.clear();
    this.loggedIn.next(false);
  }
  registerUser(data) {

    return this.http.post(this.baseUrl + "users/sign-up", data);
  }
  uploadResume(data) {

    return this.http.post(this.baseUrl + "users/upload", data);
  }
  updatePassword(data){
    var headers = this.getHeader();
    return this.http.post(this.baseUrl+"users/change",data,{headers});
  }
  updateUser(data) {
    var headers = this.getHeader();
    return this.http.post(this.baseUrl + "users/update", data, { headers });
  }
  getEditDetails(email) {
    var headers = this.getHeader();
    return this.http.get(this.baseUrl + "users/edit?emailAddress="+email, { headers });
  }

  getJobDetails(email){
    var headers = this.getHeader();
    return this.http.get(this.baseUrl + "users/job?emailAddress="+email, { headers });
  }

  postJob(data){
    var headers = this.getHeader();
    return this.http.post(this.baseUrl + "users/postjobs", data, { headers });
  }
  listJobs(email){
    //var headers = this.getHeader();
    return this.http.get(this.baseUrl + "users/getjobs?emailAddress="+email);    
  }
  filterJobs(data){
    return this.http.post(this.baseUrl + "users/filterjobs", data);
  }

  referMe(data){
    var headers = this.getHeader();
    return this.http.post(this.baseUrl + "users/referme", data, { headers });
  }

  sendOTP(emailAddress){
    return this.http.get(this.baseUrl + "users/sendOTP?emailAddress="+emailAddress);    
  }

  resetPassword(data){
    return this.http.post(this.baseUrl + "users/resetpassword", data);
  }


}
